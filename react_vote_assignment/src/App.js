import React, { useState } from 'react';
import './App.css';

const candidates = [
  { name: 'Donald Trump', votes: 0 },
  { name: 'Joe Biden', votes: 0 },
  { name: 'Seu Madruga', votes: 0 },
];

function App() {
  const [candidateList, setCandidateList] = useState(candidates);
  const [finishedCounting, setFinishedCounting] = useState(false);

  const vote = (index) => {
    setCandidateList((prevCandidates) => {
      const atualizadoCandidates = [...prevCandidates];
      atualizadoCandidates[index] = { ...prevCandidates[index], votes: prevCandidates[index].votes + 1 };
      return atualizadoCandidates;
    });
  };

  const finishCounting = () => {
    setFinishedCounting(true);
  };

  const vencedor = candidateList.reduce((prevVencedor, currentCandidate) =>
    prevVencedor.votes > currentCandidate.votes ? prevVencedor : currentCandidate
  );

  return (
    <div className="App">
      <h1>Contagem de Votos</h1>
      {!finishedCounting ? (
        <div className="candidates-list">
          {candidateList.map((candidate, index) => (
            <div key={index} className="candidate">
              <span className="candidate-name">{candidate.name}</span>
              <span className="candidate-votes">{candidate.votes}</span>
              <button onClick={() => vote(index)}>Vote!</button>
            </div>
          ))}
          <button onClick={finishCounting}>Fechar votação</button>
        </div>
      ) : (
        <div className="results">
          <h2>Resultados da Contagem de Votos</h2>
          <ul>
            {candidateList.map((candidate, index) => (
              <li key={index}>
                {candidate.name}: {candidate.votes} votos
              </li>
            ))}
          </ul>
          <p>Resultado: {vencedor.name} é o vencedor, com {vencedor.votes} votos!</p>
        </div>
      )}
    </div>
  );
}

export default App;
